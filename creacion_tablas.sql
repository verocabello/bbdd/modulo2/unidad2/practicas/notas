﻿DROP DATABASE IF EXISTS notas;
CREATE DATABASE notas;
USE notas;

CREATE OR REPLACE TABLE notas(
  id int AUTO_INCREMENT,
  fecha date,
  hora time,
  mensaje text,
  idUsuario int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE usuarios(
  id int AUTO_INCREMENT PRIMARY KEY,
  username varchar(50),
  password varchar(50),
  authKey varchar(250),
  accessToken varchar(250),
  activate tinyint(1)
  );

ALTER TABLE notas
  ADD CONSTRAINT fk_notas_usuarios
  FOREIGN KEY(idUsuario) REFERENCES usuarios(id);